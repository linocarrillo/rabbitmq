FROM rabbitmq:3.7.17-management
COPY ./conf/rabbitmq.conf /etc/rabbitmq/rabbitmq.conf
COPY ./conf/enabled_plugins /etc/rabbitmq/enabled_plugins
COPY ./conf/definitions.json /opt/definitions.json
